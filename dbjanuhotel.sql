-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 17-Jan-2019 às 18:59
-- Versão do servidor: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbjanuhotel`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `Cliente`
--
Create database dbjanuhotel;
Use dbjanuhotel;

CREATE TABLE `Cliente` (
  `CodCliente` int(11) NOT NULL,
  `Nome` varchar(50) DEFAULT NULL,
  `CPF` varchar(15) DEFAULT NULL,
  `Telefone` varchar(10) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Ativo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Compra`
--

CREATE TABLE `Compra` (
  `CodCompra` int(11) NOT NULL,
  `fk_CodReserva` int(11) DEFAULT NULL,
  `fk_CodProd` int(11) DEFAULT NULL,
  `Produto` varchar(50) DEFAULT NULL,
  `Quantidade` int(11) DEFAULT NULL,
  `VALOR` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Produto`
--

CREATE TABLE `Produto` (
  `CodProd` int(11) NOT NULL,
  `Nome` varchar(50) DEFAULT NULL,
  `Preco` double DEFAULT NULL,
  `Estoque` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Quarto`
--

CREATE TABLE `Quarto` (
  `CodQuarto` int(11) NOT NULL,
  `Tipo` varchar(50) DEFAULT NULL,
  `VALOR` double DEFAULT NULL,
  `STATUS` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Quarto_Reserva`
--

CREATE TABLE `Quarto_Reserva` (
  `CodQuarto_Reserva` int(11) NOT NULL,
  `fk_CodQuarto` int(11) DEFAULT NULL,
  `fk_CodReserva` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Reserva`
--

CREATE TABLE `Reserva` (
  `CodReserva` int(11) NOT NULL,
  `NomeCliente` varchar(50) DEFAULT NULL,
  `CodQuarto` int(11) DEFAULT NULL,
  `Data_entrada` date DEFAULT NULL,
  `Data_Saida` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Usuario`
--

CREATE TABLE `Usuario` (
  `CodUser` int(11) NOT NULL,
  `Usuario` varchar(50) DEFAULT NULL,
  `Senha` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Cliente`
--
ALTER TABLE `Cliente`
  ADD PRIMARY KEY (`CodCliente`);

--
-- Indexes for table `Compra`
--
ALTER TABLE `Compra`
  ADD PRIMARY KEY (`CodCompra`),
  ADD KEY `fk_CodReserva` (`fk_CodReserva`),
  ADD KEY `fk_CodProd` (`fk_CodProd`);

--
-- Indexes for table `Produto`
--
ALTER TABLE `Produto`
  ADD PRIMARY KEY (`CodProd`);

--
-- Indexes for table `Quarto`
--
ALTER TABLE `Quarto`
  ADD PRIMARY KEY (`CodQuarto`);

--
-- Indexes for table `Quarto_Reserva`
--
ALTER TABLE `Quarto_Reserva`
  ADD PRIMARY KEY (`CodQuarto_Reserva`),
  ADD KEY `fk_CodQuarto` (`fk_CodQuarto`),
  ADD KEY `fk_CodReserva` (`fk_CodReserva`);

--
-- Indexes for table `Reserva`
--
ALTER TABLE `Reserva`
  ADD PRIMARY KEY (`CodReserva`);

--
-- Indexes for table `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`CodUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Cliente`
--
ALTER TABLE `Cliente`
  MODIFY `CodCliente` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Compra`
--
ALTER TABLE `Compra`
  MODIFY `CodCompra` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Produto`
--
ALTER TABLE `Produto`
  MODIFY `CodProd` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Quarto`
--
ALTER TABLE `Quarto`
  MODIFY `CodQuarto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Quarto_Reserva`
--
ALTER TABLE `Quarto_Reserva`
  MODIFY `CodQuarto_Reserva` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Reserva`
--
ALTER TABLE `Reserva`
  MODIFY `CodReserva` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `CodUser` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `Compra`
--
ALTER TABLE `Compra`
  ADD CONSTRAINT `Compra_ibfk_1` FOREIGN KEY (`fk_CodReserva`) REFERENCES `Reserva` (`CodReserva`),
  ADD CONSTRAINT `Compra_ibfk_2` FOREIGN KEY (`fk_CodProd`) REFERENCES `Produto` (`CodProd`);

--
-- Limitadores para a tabela `Quarto_Reserva`
--
ALTER TABLE `Quarto_Reserva`
  ADD CONSTRAINT `Quarto_Reserva_ibfk_1` FOREIGN KEY (`fk_CodQuarto`) REFERENCES `Quarto` (`CodQuarto`),
  ADD CONSTRAINT `Quarto_Reserva_ibfk_2` FOREIGN KEY (`fk_CodReserva`) REFERENCES `Reserva` (`CodReserva`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
