package DomainModel;

import javax.persistence.*;

@Entity
@Table(name = "Produto")
public class Produto {

    @Id
    private int codProduto;
    @Column
    private String nome;
    @Column
    private Double preço;
    @Column
    private int estoque;

    public int getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(int codProduto) {
        this.codProduto = codProduto;
    }

    public String getNome() {
        return nome;
    }

   

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getPreço() {
        return preço;
    }

    public void setPreço(Double preço) {
        this.preço = preço;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }

}
