package DomainModel;

import javax.persistence.*;

@Entity
@Table(name = "Quarto")
public class Quarto {

    @Id
    @GeneratedValue
    private int codQuarto;
    @Column
    private String numero;
    @Column
    private String tipo;
    @Column
    private Double valor;
    @Column
    private String Status;

    public int getCodQuarto() {
        return codQuarto;
    }

    public void setCodQuarto(int codQuarto) {
        this.codQuarto = codQuarto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

   
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

}
