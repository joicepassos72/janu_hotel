package DomainModel;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "reserva")
public class Reserva {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cod_reserva;
    @Column
    private int cod_quarto;
    @Column
    private int cod_cliente;
    @Column
    private Date data_reserva;
    @Column
    private Date data_entrada;
    @Column
    private Date data_saida;
    @Column
    private Double custo_hospedagem;
    @Column
    private String estado;

   

    public int getCodReserva() {
        return cod_reserva;
    }

    public void setCodReserva(int cod_reserva) {
        this.cod_reserva = cod_reserva;
    }

    public int getCodQuarto() {
        return cod_quarto;
    }

    public void setCodQuarto(int cod_quarto) {
        this.cod_quarto = cod_quarto;
    }

    public int getCodCliente() {
        return cod_cliente;
    }

    public void setCodCliente(int cod_cliente) {
        this.cod_cliente = cod_cliente;
    }

    public Date getDataReserva() {
        return data_reserva;
    }

    public void setDataReserva(Date data_reserva) {
        this.data_reserva = data_reserva;
    }

    public Date getDataEntrada() {
        return data_entrada;
    }

    public void setDataEntrada(Date data_entrada) {
        this.data_entrada = data_entrada;
    }

    public Date getDataSaida() {
        return data_saida;
    }

    public void setDataSaida(Date data_saida) {
        this.data_saida = data_saida;
    }

    public Double getCusto() {
        return custo_hospedagem;
    }

    public void setCusto(Double custo_hospedagem) {
        this.custo_hospedagem = custo_hospedagem;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Object getInstance() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
