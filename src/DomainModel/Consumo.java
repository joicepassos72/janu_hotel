package DomainModel;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "consumo")
public class Consumo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cod_consumo;
    @Column
    private int cod_reserva;
    @Column
    private int cod_produto;
    @Column
    private Double quantidade;
    @Column
    private Double preco_venda;
    @Column
    private String estado;

    public Consumo() {
    }

    public Consumo(int cod_consumo, int cod_reserva, int cod_produto, Double quantidade, Double preco_venda, String estado) {
        this.cod_consumo = cod_consumo;
        this.cod_reserva = cod_reserva;
        this.cod_produto = cod_produto;
        this.quantidade = quantidade;
        this.preco_venda = preco_venda;
        this.estado = estado;
    }

    public int getCodConsumo() {
        return cod_consumo;
    }

    public void setCodConsumo(int cod_consumo) {
        this.cod_consumo = cod_consumo;
    }

    public int getCodReserva() {
        return cod_reserva;
    }

    public void setCodReserva(int cod_reserva) {
        this.cod_reserva = cod_reserva;
    }

    public int getCod_produto() {
        return cod_produto;
    }

    public void setCod_produto(int cod_produto) {
        this.cod_produto = cod_produto;
    }

    public Double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Double quantidade) {
        this.quantidade = quantidade;
    }

    public Double getPreco_venda() {
        return preco_venda;
    }

    public void setPreco_venda(Double preco_venda) {
        this.preco_venda = preco_venda;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

}
