package DataAccess;

import DomainModel.Consumo;
import java.util.*;
import javax.persistence.*;

public class ConsumoDAO {

    private static ConsumoDAO instance;
    protected EntityManager entityManager;

    public static ConsumoDAO getInstance() {
        if (instance == null) {
            instance = new ConsumoDAO();
        }
        return instance;
    }

    private ConsumoDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("HotelPU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public Consumo getById(final int id) {
        return entityManager.find(Consumo.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Consumo> findAll() {
        return entityManager.createQuery("FROM" + Consumo.class.getName()).getResultList();
    }

    public void persist(Consumo Consumo) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(Consumo);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Consumo Consumo) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(Consumo);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(Consumo Consumo) {
        try {
            entityManager.getTransaction().begin();
            Consumo = entityManager.find(Consumo.class, Consumo.getCodConsumo());
            entityManager.remove(Consumo);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final int id) {
        try {
            Consumo Consumo = getById(id);
            remove(Consumo);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
