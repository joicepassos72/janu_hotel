package DataAccess;
import DomainModel.Reserva;
import java.util.*;
import javax.persistence.*;
  

public class ReservaDAO {
    private static ReservaDAO instance;
    protected EntityManager entityManager;
    
    public static ReservaDAO getInstance(){
        if (instance == null){
            instance = new ReservaDAO();
        }
           return instance;
        }
    
    private ReservaDAO(){
             entityManager = getEntityManager();
        }
        
   
    private EntityManager getEntityManager(){
            EntityManagerFactory factory = Persistence.createEntityManagerFactory("CadCrudPU");
           if(entityManager == null){
             entityManager = factory.createEntityManager();
        }
            return entityManager;
          }
        public Reserva getById(final int id){
           return entityManager.find(Reserva.class, id);
          }
            
        @SuppressWarnings("unchecked")
         public List<Reserva> findAll(){
            return entityManager.createQuery("FROM" + Reserva.class.getName()).getResultList();
            }
        
         public void persist(Reserva Reserva){
               try{
                    entityManager.getTransaction().begin();
                    entityManager.persist(Reserva);
                    entityManager.getTransaction().commit();
                }catch(Exception ex){
                     ex.printStackTrace();
                     entityManager.getTransaction().rollback();
                 }
           }
                public void merge(Reserva Reserva){
                try{
                        entityManager.getTransaction().begin();
                        entityManager.merge(Reserva);
                        entityManager.getTransaction().commit();
                }catch(Exception ex){
                        ex.printStackTrace();
                        entityManager.getTransaction().rollback();
                }
                }
           
                public void remove(Reserva Reserva){
            try{
                    entityManager.getTransaction().begin();
                    Reserva = entityManager.find(Reserva.class, Reserva.getCodReserva());
                    entityManager.remove(Reserva);
                    entityManager.getTransaction().commit();
            }catch(Exception ex){
                    ex.printStackTrace();
                    entityManager.getTransaction().rollback();
            }
            }
           
            public void removeById(final int id){
            try{
                    Reserva reserva = getById(id);
                    remove(reserva);
            }catch(Exception ex){
                    ex.printStackTrace();
            }
            }

    public void merge() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void persist() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
            }