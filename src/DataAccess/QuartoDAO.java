package DataAccess;

import DomainModel.Quarto;
import java.util.*;
import javax.persistence.*;

public class QuartoDAO {

    private static QuartoDAO instance;
    protected EntityManager entityManager;

    public static QuartoDAO getInstance() {
        if (instance == null) {
            instance = new QuartoDAO();
        }
        return instance;
    }

    private QuartoDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("CadCrudPU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public Quarto getById(final int id) {
        return entityManager.find(Quarto.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Quarto> findAll() {
        return entityManager.createQuery("FROM" + Quarto.class.getName()).getResultList();
    }

    public void persist(Quarto Quarto) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(Quarto);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Quarto Quarto) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(Quarto);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(Quarto Quarto) {
        try {
            entityManager.getTransaction().begin();
            Quarto = entityManager.find(Quarto.class, Quarto.getCodQuarto());
            entityManager.remove(Quarto);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final int id) {
        try {
            Quarto Quarto = getById(id);
            remove(Quarto);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
