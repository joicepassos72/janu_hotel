package DataAccess;

import DomainModel.Produto;
import java.util.*;
import javax.persistence.*;

public class ProdutoDAO {

    private static ProdutoDAO instance;
    protected EntityManager entityManager;

    public static ProdutoDAO getInstance() {
        if (instance == null) {
            instance = new ProdutoDAO();
        }
        return instance;
    }

    private ProdutoDAO() {
        entityManager = getEntityManager();
    }

    private EntityManager getEntityManager() {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("CadCrudPU");
        if (entityManager == null) {
            entityManager = factory.createEntityManager();
        }
        return entityManager;
    }

    public Produto getById(final int id) {
        return entityManager.find(Produto.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Produto> findAll() {
        return entityManager.createQuery("FROM" + Produto.class.getName()).getResultList();
    }

    public void persist(Produto Produto) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(Produto);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void merge(Produto Produto) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(Produto);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void remove(Produto Produto) {
        try {
            entityManager.getTransaction().begin();
            Produto = entityManager.find(Produto.class, Produto.getCodProduto());
            entityManager.remove(Produto);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    public void removeById(final int id) {
        try {
            Produto Produto = getById(id);
            remove(Produto);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
